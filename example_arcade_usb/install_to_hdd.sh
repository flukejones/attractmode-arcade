#!/bin/bash

cp -rfv ./art           $HOME/art
cp -rfv ./attractmode   $HOME/.attract
cp -rfv ./mame          $HOME/mame

cat 'attract' >> $HOME/.config/openbox/autostart

sed -i 's:autostart &:#autostart &:g' $HOME/.xinitrc
sed -i 's:autokill &:#autostart &:g' $HOME/.xinitrc

sed -i 's:/media/arcade_usb:$HOME:g' $HOME/.attract/emulators/*
